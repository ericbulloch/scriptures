#!/bin/bash

# Exit immediately if a command exits with a nonzero status.
set -e

# Helper subroutines to help with logging.
info () { printf "[ \033[00;34mINFO\033[0m ] $1\n"; }
success () { printf "\r[ \033[00;32mSUCCESS\033[0m ] $1\n"; }
fail () { printf "\r[\033[0;31mFAIL\033[0m] $1\n"; exit; }

info "Welcome to the scriptures project!"
info "Lets get the development environment setup."
info "Checking if the virtual environment is set up."
if [ -d "bin" ]; then
  success "The virtual environment is already set up."
else
  virtualenv --python=python3 .
  success "The virtual environment is now ready to go!"
fi

info "Installing the requirements."
bin/pip install -r requirements.txt
success "Done installing the requirements!"

info "Setting up the database."
bin/python main.py get
success "Setting up the database."

success "Your development environment is not set up."
success "Please activate the virtual environment with: 'source bin/activate'."
success "After it is activated, run 'FLASK_APP=server.py flask run' to start taking requests."

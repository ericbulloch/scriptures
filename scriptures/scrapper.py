from collections import OrderedDict
import glob
import os

from bs4 import BeautifulSoup
import requests


SCRIPTURES = OrderedDict([
    ('OLD TESTAMENT', {'ot':
        OrderedDict([
            ('GENESIS', ('gen', 50)),
            ('EXODUS', ('ex', 40)),
            ('LEVITICUS', ('lev', 27)),
            ('NUMBERS', ('num', 36)),
            ('DEUTERONOMY', ('deut', 34)),
            ('JOSHUA', ('josh', 24)),
            ('JUDGES', ('judg', 21)),
            ('RUTH', ('ruth', 4)),
            ('1 SAMUEL', ('1-sam', 31)),
            ('2 SAMUEL', ('2-sam', 24)),
            ('1 KINGS', ('1-kgs', 22)),
            ('2 KINGS', ('2-kgs', 25)),
            ('1 CHRONICLES', ('1-chr', 29)),
            ('2 CHRONICLES', ('2-chr', 36)),
            ('EZRA', ('ezra', 10)),
            ('NEHEMIAH', ('neh', 13)),
            ('ESTHER', ('esth', 10)),
            ('JOB', ('job', 42)),
            ('PSALMS', ('ps', 150)),
            ('THE PROVERBS', ('prov', 31)),
            ('ECCLESIASTES', ('eccl', 12)),
            ('ISAIAH', ('isa', 66)),
            ('JEREMIAH', ('jer', 52)),
            ('LAMENTATIONS', ('lam', 5)),
            ('EZEKIEL', ('ezek', 48)),
            ('DANIEL', ('dan', 12)),
            ('HOSEA', ('hosea', 14)),
            ('JOEL', ('joel', 3)),
            ('AMOS', ('amos', 9)),
            ('OBADIAH', ('obad', 1)),
            ('JONAH', ('jonah', 4)),
            ('MICAH', ('micah', 7)),
            ('NAHUM', ('nahum', 3)),
            ('HABAKKUK', ('hab', 3)),
            ('ZEPHANIAH', ('zeph', 3)),
            ('HAGGAI', ('hag', 2)),
            ('ZECHARIAH', ('zech', 14)),
            ('MALACHI', ('mal', 4)),
        ])
    }),
    ('NEW TESTAMENT', {'nt':
        OrderedDict([
            ('ST MATTHEW', ('matt', 28)),
            ('ST MARK', ('mark', 16)),
            ('ST LUKE', ('luke', 24)),
            ('ST JOHN', ('john', 21)),
            ('THE ACTS OF THE APOSTLES', ('acts', 28)),
            ('ROMANS', ('rom', 16)),
            ('1 CORINTHIANS', ('1-cor', 16)),
            ('2 CORINTHIANS', ('2-cor', 13)),
            ('GALATIANS', ('gal', 6)),
            ('EPHESIANS', ('eph', 6)),
            ('PHILIPPIANS', ('philip', 4)),
            ('COLOSSIANS', ('col', 4)),
            ('1 THESSALONIANS', ('1-thes', 5)),
            ('2 THESSALONIANS', ('2-thes', 3)),
            ('1 TIMOTHY', ('1-tim', 6)),
            ('2 TIMOTHY', ('2-tim', 4)),
            ('TITUS', ('titus', 3)),
            ('PHILEMON', ('philem', 1)),
            ('HEBREWS', ('heb', 13)),
            ('JAMES', ('james', 5)),
            ('1 PETER', ('1-pet', 5)),
            ('2 PETER', ('2-pet', 3)),
            ('1 JOHN', ('1-jn', 5)),
            ('2 JOHN', ('2-jn', 1)),
            ('3 JOHN', ('3-jn', 1)),
            ('JUDE', ('jude', 1)),
            ('THE REVELATION', ('rev', 22)),
        ])
    }),
    ('BOOK OF MORMON', {'bofm':
        OrderedDict([
            ('1 NEPHI', ('1-ne', 22)),
            ('2 NEPHI', ('2-ne', 33)),
            ('JACOB', ('jacob', 7)),
            ('ENOS', ('enos', 1)),
            ('JAROM', ('jarom', 1)),
            ('OMNI', ('omni', 1)),
            ('THE WORDS OF MORMON', ('w-of-m', 1)),
            ('MOSIAH', ('mosiah', 29)),
            ('ALMA', ('alma', 63)),
            ('HELAMAN', ('hel', 16)),
            ('3 NEPHI', ('3-ne', 30)),
            ('4 NEPHI', ('4-ne', 1)),
            ('MORMON', ('morm', 9)),
            ('ETHER', ('ether', 15)),
            ('MORONI', ('moro', 10)),
        ])
    }),
    ('DOCTRINE AND COVENANTS', {'dc-testament':
        OrderedDict([
            ('SECTION 1', ('dc', 1)),
            ('SECTION 2', ('dc', 1)),
            ('SECTION 3', ('dc', 1)),
            ('SECTION 4', ('dc', 1)),
            ('SECTION 5', ('dc', 1)),
            ('SECTION 6', ('dc', 1)),
            ('SECTION 7', ('dc', 1)),
            ('SECTION 8', ('dc', 1)),
            ('SECTION 9', ('dc', 1)),
            ('SECTION 10', ('dc', 1)),
            ('SECTION 11', ('dc', 1)),
            ('SECTION 12', ('dc', 1)),
            ('SECTION 13', ('dc', 1)),
            ('SECTION 14', ('dc', 1)),
            ('SECTION 15', ('dc', 1)),
            ('SECTION 16', ('dc', 1)),
            ('SECTION 17', ('dc', 1)),
            ('SECTION 18', ('dc', 1)),
            ('SECTION 19', ('dc', 1)),
            ('SECTION 20', ('dc', 1)),
            ('SECTION 21', ('dc', 1)),
            ('SECTION 22', ('dc', 1)),
            ('SECTION 23', ('dc', 1)),
            ('SECTION 24', ('dc', 1)),
            ('SECTION 25', ('dc', 1)),
            ('SECTION 26', ('dc', 1)),
            ('SECTION 27', ('dc', 1)),
            ('SECTION 28', ('dc', 1)),
            ('SECTION 29', ('dc', 1)),
            ('SECTION 30', ('dc', 1)),
            ('SECTION 31', ('dc', 1)),
            ('SECTION 32', ('dc', 1)),
            ('SECTION 33', ('dc', 1)),
            ('SECTION 34', ('dc', 1)),
            ('SECTION 35', ('dc', 1)),
            ('SECTION 36', ('dc', 1)),
            ('SECTION 37', ('dc', 1)),
            ('SECTION 38', ('dc', 1)),
            ('SECTION 39', ('dc', 1)),
            ('SECTION 40', ('dc', 1)),
            ('SECTION 41', ('dc', 1)),
            ('SECTION 42', ('dc', 1)),
            ('SECTION 43', ('dc', 1)),
            ('SECTION 44', ('dc', 1)),
            ('SECTION 45', ('dc', 1)),
            ('SECTION 46', ('dc', 1)),
            ('SECTION 47', ('dc', 1)),
            ('SECTION 48', ('dc', 1)),
            ('SECTION 49', ('dc', 1)),
            ('SECTION 50', ('dc', 1)),
            ('SECTION 51', ('dc', 1)),
            ('SECTION 52', ('dc', 1)),
            ('SECTION 53', ('dc', 1)),
            ('SECTION 54', ('dc', 1)),
            ('SECTION 55', ('dc', 1)),
            ('SECTION 56', ('dc', 1)),
            ('SECTION 57', ('dc', 1)),
            ('SECTION 58', ('dc', 1)),
            ('SECTION 59', ('dc', 1)),
            ('SECTION 60', ('dc', 1)),
            ('SECTION 61', ('dc', 1)),
            ('SECTION 62', ('dc', 1)),
            ('SECTION 63', ('dc', 1)),
            ('SECTION 64', ('dc', 1)),
            ('SECTION 65', ('dc', 1)),
            ('SECTION 66', ('dc', 1)),
            ('SECTION 67', ('dc', 1)),
            ('SECTION 68', ('dc', 1)),
            ('SECTION 69', ('dc', 1)),
            ('SECTION 70', ('dc', 1)),
            ('SECTION 71', ('dc', 1)),
            ('SECTION 72', ('dc', 1)),
            ('SECTION 73', ('dc', 1)),
            ('SECTION 74', ('dc', 1)),
            ('SECTION 75', ('dc', 1)),
            ('SECTION 76', ('dc', 1)),
            ('SECTION 77', ('dc', 1)),
            ('SECTION 78', ('dc', 1)),
            ('SECTION 79', ('dc', 1)),
            ('SECTION 80', ('dc', 1)),
            ('SECTION 81', ('dc', 1)),
            ('SECTION 82', ('dc', 1)),
            ('SECTION 83', ('dc', 1)),
            ('SECTION 84', ('dc', 1)),
            ('SECTION 85', ('dc', 1)),
            ('SECTION 86', ('dc', 1)),
            ('SECTION 87', ('dc', 1)),
            ('SECTION 88', ('dc', 1)),
            ('SECTION 89', ('dc', 1)),
            ('SECTION 90', ('dc', 1)),
            ('SECTION 91', ('dc', 1)),
            ('SECTION 92', ('dc', 1)),
            ('SECTION 93', ('dc', 1)),
            ('SECTION 94', ('dc', 1)),
            ('SECTION 95', ('dc', 1)),
            ('SECTION 96', ('dc', 1)),
            ('SECTION 97', ('dc', 1)),
            ('SECTION 98', ('dc', 1)),
            ('SECTION 99', ('dc', 1)),
            ('SECTION 100', ('dc', 1)),
            ('SECTION 101', ('dc', 1)),
            ('SECTION 102', ('dc', 1)),
            ('SECTION 103', ('dc', 1)),
            ('SECTION 104', ('dc', 1)),
            ('SECTION 105', ('dc', 1)),
            ('SECTION 106', ('dc', 1)),
            ('SECTION 107', ('dc', 1)),
            ('SECTION 108', ('dc', 1)),
            ('SECTION 109', ('dc', 1)),
            ('SECTION 110', ('dc', 1)),
            ('SECTION 111', ('dc', 1)),
            ('SECTION 112', ('dc', 1)),
            ('SECTION 113', ('dc', 1)),
            ('SECTION 114', ('dc', 1)),
            ('SECTION 115', ('dc', 1)),
            ('SECTION 116', ('dc', 1)),
            ('SECTION 117', ('dc', 1)),
            ('SECTION 118', ('dc', 1)),
            ('SECTION 119', ('dc', 1)),
            ('SECTION 120', ('dc', 1)),
            ('SECTION 121', ('dc', 1)),
            ('SECTION 122', ('dc', 1)),
            ('SECTION 123', ('dc', 1)),
            ('SECTION 124', ('dc', 1)),
            ('SECTION 125', ('dc', 1)),
            ('SECTION 126', ('dc', 1)),
            ('SECTION 127', ('dc', 1)),
            ('SECTION 128', ('dc', 1)),
            ('SECTION 129', ('dc', 1)),
            ('SECTION 130', ('dc', 1)),
            ('SECTION 131', ('dc', 1)),
            ('SECTION 132', ('dc', 1)),
            ('SECTION 133', ('dc', 1)),
            ('SECTION 134', ('dc', 1)),
            ('SECTION 135', ('dc', 1)),
            ('SECTION 136', ('dc', 1)),
            ('SECTION 137', ('dc', 1)),
            ('SECTION 138', ('dc', 1)),
        ])
    }),
    ('PEARL OF GREAT PRICE', {'pgp':
        OrderedDict([
            ('MOSES', ('moses', 8)),
            ('ABRAHAM', ('abr', 5)),
            ('JOSEPH SMITH - MATTHEW', ('js-m', 1)),
            ('JOSEPH SMITH - HISTORY', ('js-h', 1)),
            ('THE ARTICLES OF FAITH', ('a-of-f', 1)),
        ])
    }),
])


def get_scriptures():
    """Get all scriptures."""
    return [s for s in SCRIPTURES.keys()]


def get_scripture_from_book(book):
    """Get scripture based on book."""
    for scripture in SCRIPTURES.keys():
        for _, items in SCRIPTURES[scripture].items():
            for possible_book in items.keys():
                if possible_book == book:
                    return scripture
    return None 



def get_books(scripture):
    """Get all books."""
    return list([b.keys() for _, b in SCRIPTURES[scripture].items()][0])


def get_chapters(scripture, book):
    """Get all chapters."""
    return [b for _, b in SCRIPTURES[scripture].items()][0][book][1]


def get_chapter(scripture, book, chapter):
    """Get all the verses in the given chapter."""
    base_directory = 'store'
    for directory in [scripture, book, chapter]:
        base_directory = os.path.join(base_directory, str(directory))
        if not os.path.exists(base_directory):
            return get_chapter_from_webservice(scripture, book, chapter)
    files = glob.glob(base_directory + '/*.txt')
    files.sort()
    verses = []
    for file in files:
        with open(file, 'r') as fp:
            verses.append(' '.join(fp.readlines()))
    return verses


def get_chapter_from_webservice(scripture, book, chapter):
    """Pull a chapter from the webservice."""
    scripture_key = list(SCRIPTURES[scripture].keys())[0]
    book_key = SCRIPTURES[scripture][scripture_key][book][0]
    url = 'https://www.lds.org/scriptures/{scripture}/{book}/{chapter}'
    print(url.format(scripture=scripture_key, book=book_key, chapter=chapter))
    response = requests.get(url.format(scripture=scripture_key, book=book_key,
                                       chapter=chapter))
    html = response.text
    soup = BeautifulSoup(html.encode('utf-8'), 'html.parser')
    verses = []
    for i in range(1, 201):
        divs = soup.select('p#p%s' % i)
        if not divs:
            break
        for tag in ['a', 'span']: 
            for match in divs[0].findAll(tag):
                match.replace_with_children()
        for tag in ['sup']:
            for match in divs[0].findAll(tag):
                match.replace_with('')
        verse_text = divs[0].get_text().encode('utf-8').decode('utf-8')
        text = verse_text[len('%s ' % i):]
        save(scripture, book, chapter, i, text)
        verses.append(text)
    return verses


def save(scripture, book, chapter, verse, text):
    """Save the text locally."""
    file_path = 'store'
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    file_path = '%s/%s' % (file_path, scripture)
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    file_path = '%s/%s' % (file_path, book)
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    file_path = '%s/%s' % (file_path, chapter)
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    file_path = '%s/%s.txt' % (file_path, str(verse).zfill(3))
    with open(file_path, 'w+') as file_pointer:
        file_pointer.write(text)

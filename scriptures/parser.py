import re


CONVERSIONS = {
    'D&C': 'DOCTRINE AND COVENANTS',
    'WOFM': 'WORDS OF MORMON',
    'AOF': 'THE ARTICLES OF FAITH',
    'MATTHEW': 'ST MATTHEW',
    'MARK': 'ST MARK',
    'LUKE': 'ST LUKE',
    'JOHN': 'ST JOHN',
}

VERSES_REGEX = re.compile(r'\d+')
VERSE_OPERATORS_REGEX = re.compile(r'[,-]')


def get_chapter(reference):
    """Get the chapter from the reference."""
    index = None
    for i in range(len(reference) - 1, -1, -1):
        if not reference[i].isdigit():
            index = i
            break
    return index + 1, int(reference[index + 1:])


def get_verses(reference):
    """Get the verses from the reference."""
    potential_verses = reference.split(':')[1]
    operators = re.findall(VERSE_OPERATORS_REGEX, potential_verses)
    operands = [int(i) for i in re.findall(VERSES_REGEX, potential_verses)]
    operators_index = operands_index = 0
    verses = []
    ops = []
    operator = None
    while True:
        if operands_index >= len(operands):
            break
        ops.append(operands[operands_index])
        operands_index += 1
        if len(operators) > operators_index:
            operator = operators[operators_index]
            if operator == ',':
                operators_index += 1
                verses.extend(ops)
                ops = []
            if operator == '-':
                operators_index += 1
                ops.append(operands[operands_index])
                operands_index += 1
                verses.extend(range(ops[0], ops[1] + 1))
                ops = []
        else:
            verses.extend(ops)
            break
    return reference.index(':'), verses


def get_next_digit_index(reference):
    """Get the next index of a digit."""
    for i in range(len(reference) - 1, -1, -1):
        if reference[i].isdigit():
            return i
    return None


def parse(reference):
    """Parse the provided scripture reference."""
    reference = reference.upper()
    index, verses = get_verses(reference)
    ending_chapter_index = get_next_digit_index(reference[:index])
    index, chapter = get_chapter(reference[:ending_chapter_index + 1])
    book = reference[:index].strip().replace('-', '')
    if book in CONVERSIONS:
        book = CONVERSIONS[book]
    if book == 'DOCTRINE AND COVENANTS':
        chapter = 'SECTION %s' % chapter
    return [book, chapter, verses]
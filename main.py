import sys

from scriptures.parser import parse
from scriptures.scrapper import get_books, get_chapter, \
    get_chapters, get_scripture_from_book, get_scriptures, SCRIPTURES


def get_all():
    """Pull all scriptures down from the site."""
    for scripture in get_scriptures():
        for book in get_books(scripture):
            for chapter in range(1, get_chapters(scripture, book) + 1):
                if scripture == 'DOCTRINE AND COVENANTS':
                    chapter = int(book.split()[1])
                get_chapter(scripture, book, chapter)


def main(reference=None):
    """Get the provided scriptures."""
    if reference is None:
        reference = input('Please enter a scripture reference:  ')
    selection = parse(reference)
    if selection[0] == 'DOCTRINE AND COVENANTS':
        scripture = selection[0]
        book = selection[1]
        chapter = int(book.split()[1])
    else:
        scripture = get_scripture_from_book(selection[0])
        book = selection[0]
        chapter = selection[1]
    all_verses = get_chapter(scripture, book, chapter)
    for verse in selection[2]:
        try:
            print('%s)' % verse, all_verses[verse - 1])
        except IndexError:
            break



if __name__ == '__main__':
    param = ' '.join(sys.argv[1:]) if len(sys.argv) > 1 else None
    if param == 'get':
        get_all()
    else:
        main(param)

import pytest

from scriptures.scriptures.parser import parse


@pytest.mark.parametrize('expected_value, input', [
    (['1 NEPHI', 15, [24]], '1 NEPHI 15:24'),
    (['1 NEPHI', 15, [24]], '1 nephi 15:24'),
    (['1 NEPHI', 15, [24]], '1 nephi 15 : 24'),
    (['1 NEPHI', 15, [24]], '1 nephi 15 :24'),
    (['1 NEPHI', 15, [24]], '1 nephi 15: 24'),
    (['1 NEPHI', 15, [24]], '1 nephi 15:24 '),
    (['1 NEPHI', 15, [24]], '1 nephi  15:24'),
    (['1 NEPHI', 3, [7]], '1 NEPHI 3:7'),
    (['3 NEPHI', 11, [10, 11, 12, 13, 14]], '3 NEPHI 11:10-14'),
    (['ALMA', 26, [27]], 'ALMA 26:27'),
    (['MOSES', 1, [39]], 'MOSES 1:39'),
    (['WORDS OF MORMON', 1, [7]], 'words of mormon 1:7'),
    (['WORDS OF MORMON', 1, [7]], 'wofm 1:7'),
    (['WORDS OF MORMON', 1, [7]], 'w-of-m 1:7'),
    (['ALMA', 61, [12, 13]], 'alma 61:12-13'),
    (['ALMA', 61, [12, 13]], 'alma 61:12- 13'),
    (['ALMA', 61, [12, 13]], 'alma 61:12 -13'),
    (['ALMA', 61, [12, 13]], 'alma 61 : 12- 13'),
    (['ALMA', 61, [12, 13]], 'alma 61: 12-13'),
    (['ALMA', 61, [12, 13]], 'alma 61 :12-13'),
    (['DOCTRINE AND COVENANTS', 'SECTION 14', [7]], 'd&c 14:7'),
    (['DOCTRINE AND COVENANTS', 'SECTION 18', [10, 15, 16]], 'DOCTRINE AND COVENANTS 18:10,15-16'),
    (['DOCTRINE AND COVENANTS', 'SECTION 18', [10, 15, 16, 17]], 'DOCTRINE AND COVENANTS 18:10,15-17'),
    (['THE ARTICLES OF FAITH', 1, [1, 2, 3, 4]], 'aof 1:1-4'),
    (['THE ARTICLES OF FAITH', 1, [1, 2, 3, 4]], 'a-o-f 1:1-4'),
])
def test_parse(expected_value, input):
    """Test the parse function."""
    result = parse(input)

    assert expected_value == result
